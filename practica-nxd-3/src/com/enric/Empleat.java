package com.enric;


public class Empleat {

	int emp_no;
	String apellido;
	String oficio;
	int dir;
	String fecha_alt;
	int salario;
	int dept_no;
	
	
	public Empleat() {
		
	}


	public Empleat(int emp_no, String apellido, String oficio, int dir,
			String fecha_alt, int salario, int dept_no) {
		super();
		this.emp_no = emp_no;
		this.apellido = apellido;
		this.oficio = oficio;
		this.dir = dir;
		this.fecha_alt = fecha_alt;
		this.salario = salario;
		this.dept_no = dept_no;
	}


	public int getEmp_no() {
		return emp_no;
	}


	public void setEmp_no(int emp_no) {
		this.emp_no = emp_no;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public String getOficio() {
		return oficio;
	}


	public void setOficio(String oficio) {
		this.oficio = oficio;
	}


	public int getDir() {
		return dir;
	}


	public void setDir(int dir) {
		this.dir = dir;
	}


	public String getFecha_alt() {
		return fecha_alt;
	}


	public void setFecha_alt(String fecha_alt) {
		this.fecha_alt = fecha_alt;
	}


	public int getSalario() {
		return salario;
	}


	public void setSalario(int salario) {
		this.salario = salario;
	}


	public int getDept_no() {
		return dept_no;
	}


	public void setDept_no(int dept_no) {
		this.dept_no = dept_no;
	}
	
	
	
}
