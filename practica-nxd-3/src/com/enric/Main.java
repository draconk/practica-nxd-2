package com.enric;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class Main {

	private final static String fichero = "D:\\perfiles\\Users\\AMS2-04\\Documents\\proyectos java\\practica-nxd-2\\practica-nxd-3\\docs/empleados.json";
	static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	
	public static void main(String[] args) {
		try {
			veremple();
		} catch (FileNotFoundException e) {
			
			e.printStackTrace();
		}
	}

	public static Empleat[] veremple() throws FileNotFoundException{

        Empleat[] empleats;
		String json = null;
		BufferedReader br = new BufferedReader(new FileReader(fichero));
		StringBuilder sb = new StringBuilder();
		String line;
		try {
			line = br.readLine();


			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}
			json = sb.toString();
		} catch (IOException e) {
			
			e.printStackTrace();
		
		}

		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		empleats = gson.fromJson(json, Empleat[].class);
		
		System.out.println(gson.toJson(empleats));
		
		
		return empleats;
	}

	public static void Insertadep() throws IOException {
		
		int emp_no;
		String apellido;
		String oficio;
		int dir;
		String fecha_alt;
		int salario;
		int dept_no;

        System.out.println("Numero de empleado: ");
        emp_no = in.read();
        System.out.println("Apellido: ");
        apellido = in.readLine();
        System.out.println("Oficio: ");
        oficio = in.readLine();
        System.out.println("Direccion: ");
        dir = in.read();
        System.out.println("Fecha alta: dd/mm/yyyy");
        fecha_alt = in.readLine();
        System.out.printf("Salario: ");
        salario = in.read();
        System.out.println("Numero de departamento: ");
        dept_no = in.read();


    }

}
