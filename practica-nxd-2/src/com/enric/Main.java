package com.enric;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import javax.xml.xquery.XQConnection;
import javax.xml.xquery.XQDataSource;
import javax.xml.xquery.XQException;
import javax.xml.xquery.XQPreparedExpression;
import javax.xml.xquery.XQResultSequence;
import net.xqj.exist.ExistXQDataSource;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.modules.CollectionManagementService;
import org.xmldb.api.modules.XMLResource;


public class Main {
	
	public final static String URI = "xmldb:exist://localhost:8080/exist/xmlrpc";
	
	public static void main(String[] args) throws Exception {
		System.out.println("1. Subir xml a eXist");
		System.out.println("2. Guardar la zona 20 en zona20.xml");
		
		Scanner scn = new Scanner(System.in);
		String opcion = scn.nextLine();
		
		switch (Integer.parseInt(opcion)) {
		case 1:
			SubirXml();
			break;
		case 2:
			GuardarZone20();
			break;
		default:
			break;
		}
	}
	
	
	public static void SubirXml() throws Exception{
		String collection = "/db/ColeccionZones", file1 = "ColeccionZones/zones.xml", file2 = "ColeccionZones/productos.xml";

        // initialize driver
        String driver = "org.exist.xmldb.DatabaseImpl";
        Class cl = Class.forName(driver);
        Database database = (Database)cl.newInstance();
        DatabaseManager.registerDatabase(database);

        // try to get collection
        System.out.println(URI + collection);
        Collection col = DatabaseManager.getCollection(URI + collection, "admin", "");
        
 
        if(col == null) {           
            Collection root = DatabaseManager.getCollection(URI + "/db", "admin", "");
            CollectionManagementService mgtService = (CollectionManagementService) root.getService("CollectionManagementService", "1.0");
            col = mgtService.createCollection(collection.substring("/db".length()));
        }
        // Guarda el primer archivo
        XMLResource document = (XMLResource)col.createResource(file1.substring(file1.lastIndexOf('/')), "XMLResource");
        File f = new File(file1);
        if(!f.canRead()) {
            System.out.println("cannot read file " + file1);
            return;
        }
        document.setContent(f);
        System.out.print("storing document " + document.getId() + "...");
        col.storeResource(document);
        System.out.println("ok.");
        
        //Guarda el segundo archivo
        document = (XMLResource)col.createResource(file2.substring(file2.lastIndexOf('/')), "XMLResource");
        f = new File(file2);
        if(!f.canRead()) {
            System.out.println("cannot read file " + file2);
            return;
        }        
        document.setContent(f);
        System.out.print("storing document " + document.getId() + "...");
        col.storeResource(document);
        System.out.println("ok.");
	}
	
	
	
	public static void GuardarZone20(){
		String outFile = "ColeccionZones/zones20.xml";
		XQDataSource server = new ExistXQDataSource();
		String query = "<productos>"+    
						"{"+
						    "for $productos in doc('ColeccionZones/productos.xml')//productos/produc[cod_zona = 20],"+
						    "$zonas in doc('ColeccionZones/zones.xml')//zonas/zona[cod_zona = 20]"+
						    "return <zona20>"+
						            "{"+
						                "$productos/cod_prod,"+
						                "$productos/denominacion,"+
						                "$productos/precio,"+
						                "$zonas/nombre,"+
						                "$zonas/director,"+
						                "<stock>"+
						                "{$productos/stock_actual - $productos/stock_minimo}"+
						                "</stock>"+
						            "}"+
						          "</zona20>"+
						"}"+
						"</productos>";
		
		try {
			server.setProperty("serverName","localhost");
			server.setProperty("port","8080");
			server.setProperty("user","admin");
			server.setProperty("password","");
			
			XQConnection conn = server.getConnection();
			XQPreparedExpression consulta = conn.prepareExpression(query);
			XQResultSequence result = consulta.executeQuery();
			
			BufferedWriter bw = new BufferedWriter(new FileWriter(outFile));
			bw.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>"+"\n");
			while (result.next()){
				String cad = result.getItemAsString(null);
				System.out.println("output: \n"+cad);
				bw.write(cad+"\n");
			}
			bw.close();
		} catch (XQException e) {
			e.printStackTrace();
		} catch (IOException ioe){
			ioe.printStackTrace();
		}
	}
}


